package service

import (
	"fmt"

	"github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/config"
	"github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/genproto/auth_service"
	catalog_service "github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/genproto/catalog_service"
	order_service "github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/genproto/order_service"

	"google.golang.org/grpc"
)

type ServiceManager interface {
	BannerService() catalog_service.BannerServiceClient
	CategoryService() catalog_service.CategoryServiceClient
	ProductService() catalog_service.ProductServiceClient

	OrderService() order_service.OrderServiceClient
	CartService() order_service.CartServiceClient

	AuthService() auth_service.AuthServiceClient
	PermissionService() auth_service.PermissionServiceClient
	UserRoleService() auth_service.UserRoleServiceClient
	UserRolePermissionService() auth_service.UserRolePermissionServiceClient
}

type grpcClients struct {
	bannerService   catalog_service.BannerServiceClient
	categoryService catalog_service.CategoryServiceClient
	productService  catalog_service.ProductServiceClient

	orderService order_service.OrderServiceClient
	cartService order_service.CartServiceClient


	authService               auth_service.AuthServiceClient
	permissionService         auth_service.PermissionServiceClient
	userRoleService           auth_service.UserRoleServiceClient
	userRolePermissionService auth_service.UserRolePermissionServiceClient
}

func NewGrpcClients(cfg *config.Config) (ServiceManager, error) {
	connCatalogService, err := grpc.Dial(fmt.Sprintf("%s:%d", cfg.CatalogServiceHost, cfg.CatalogServicePort), grpc.WithInsecure())
	if err != nil {
		return nil, err
	}
	connOrderService, err := grpc.Dial(fmt.Sprintf("%s:%d", cfg.OrderServiceHost, cfg.OrderServicePort), grpc.WithInsecure())
	if err != nil {
		return nil, err
	}
	connAuthService, err := grpc.Dial(fmt.Sprintf("%s:%d", cfg.AuthServiceHost, cfg.AuthServicePort), grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		bannerService:   catalog_service.NewBannerServiceClient(connCatalogService),
		categoryService: catalog_service.NewCategoryServiceClient(connCatalogService),
		productService:  catalog_service.NewProductServiceClient(connCatalogService),

		orderService:    order_service.NewOrderServiceClient(connOrderService),
		cartService: order_service.NewCartServiceClient(connOrderService),

		authService:               auth_service.NewAuthServiceClient(connAuthService),
		permissionService:         auth_service.NewPermissionServiceClient(connAuthService),
		userRoleService:           auth_service.NewUserRoleServiceClient(connAuthService),
		userRolePermissionService: auth_service.NewUserRolePermissionServiceClient(connAuthService),
	}, nil
}

func (g *grpcClients) BannerService() catalog_service.BannerServiceClient {
	return g.bannerService
}

func (g *grpcClients) CategoryService() catalog_service.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) ProductService() catalog_service.ProductServiceClient {
	return g.productService
}

func (g *grpcClients) OrderService() order_service.OrderServiceClient {
	return g.orderService
}
func (g *grpcClients) CartService() order_service.CartServiceClient {
	return g.cartService
}

func (g *grpcClients) AuthService() auth_service.AuthServiceClient {
	return g.authService
}
func (g *grpcClients) PermissionService() auth_service.PermissionServiceClient {
	return g.permissionService
}
func (g *grpcClients) UserRoleService() auth_service.UserRoleServiceClient {
	return g.userRoleService
}
func (g *grpcClients) UserRolePermissionService() auth_service.UserRolePermissionServiceClient {
	return g.userRolePermissionService
}
