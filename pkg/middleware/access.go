package middleware

import (
	"github.com/gin-gonic/gin"
)

func HasAccess(userType string) gin.HandlerFunc {
	return func(c *gin.Context) {
		// clientID := c.GetHeader("client-id")
		// if userType == "SYSTEM_USER" && clientID != config.SystemUserID {
		// 	c.AbortWithStatus(403)
		// } else if userType == "TUTOR" && clientID != config.TutorID {
		// 	c.AbortWithStatus(403)
		// } else if userType == "STUDENT" && clientID != config.StudentID {
		// 	c.AbortWithStatus(403)
		// } else if userType == "SYSTEM_USER_TUTOR" && (clientID != config.SystemUserID && clientID != config.TutorID) {
		// 	c.AbortWithStatus(403)
		// } else if userType == "SUPER_USER_SYSTEM_USER_TUTOR" && (clientID != config.SystemUserID && clientID != config.TutorID && clientID != config.SuperUserID) {
		// 	c.AbortWithStatus(403)
		// } else if userType == "SUPER_USER_SYSTEM_USER" && (clientID != config.SystemUserID && clientID != config.SuperUserID) {
		// 	c.AbortWithStatus(403)
		// } else if userType == "SUPER_USER" && clientID != config.SuperUserID {
		// 	c.AbortWithStatus(403)
		// }
		c.Next()
	}
}
