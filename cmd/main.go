package main

import (
	"fmt"

	"github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/api"
	"github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/config"
	"github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/pkg/logger"
	"github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/service"
)

func main() {
	cfg := config.Load()
	fmt.Printf("cfg:::%+v\n", cfg)
	log := logger.New(cfg.LogLevel, "nt_api_gateway")
	grpcClients, err := service.NewGrpcClients(&cfg)

	if err != nil {
		log.Error("error while connecting clients")
		return
	}

	server := api.New(&api.RouterOptions{
		Log:      log,
		Cfg:      &cfg,
		Services: grpcClients,
	})

	server.Run(cfg.HttpPort)
}
