package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)


type Config struct {
	Environment string // development, staging, production
	DefaultLang string // uz, ru, en

	LogLevel string
	HttpPort string

	CatalogServiceHost string
	CatalogServicePort int

	OrderServiceHost string
	OrderServicePort int

	AuthServiceHost string
	AuthServicePort int

	DefaultOffset string
	DefaultLimit  string
}

func Load() Config {
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
	config := Config{}

	config.Environment = cast.ToString(getOrReturnDefaultValue("ENVIRONMENT", "production"))
	config.DefaultLang = cast.ToString(getOrReturnDefaultValue("DEFAULT_LANG", "ru"))
	
	config.LogLevel = cast.ToString(getOrReturnDefaultValue("LOG_LEVEL", "debug"))
	config.HttpPort = cast.ToString(getOrReturnDefaultValue("HTTP_PORT", ":8080"))

	config.CatalogServiceHost = cast.ToString(getOrReturnDefaultValue("CATALOG_SERVICE_HOST", "localhost"))
	config.CatalogServicePort = cast.ToInt(getOrReturnDefaultValue("CATALOG_SERVICE_PORT", 9102))

	config.OrderServiceHost = cast.ToString(getOrReturnDefaultValue("ORDER_SERVICE_HOST", "localhost"))
	config.OrderServicePort = cast.ToInt(getOrReturnDefaultValue("ORDER_SERVICE_PORT", 9002))

	config.AuthServiceHost = cast.ToString(getOrReturnDefaultValue("AUTH_SERVICE_HOST", "localhost"))
	config.AuthServicePort = cast.ToInt(getOrReturnDefaultValue("AUTH_SERVICE_PORT", 9003))

	config.DefaultOffset = cast.ToString(getOrReturnDefaultValue("DEFAULT_OFFSET", "0"))
	config.DefaultLimit = cast.ToString(getOrReturnDefaultValue("DEFAULT_LIMIT", "10"))

	
	return config
}

func getOrReturnDefaultValue(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)

	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
