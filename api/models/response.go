package models

type CreatedResponse struct {
	ID string `json:"id"`
}

type FailureResponse struct {
	Error string `json:"error"`
}

type UploadResponse struct {
	Filename string `json:"filename"`
}

type ResponseSuccess struct {
	Metadata interface{}
	Data     interface{}
}

type ResponseError struct {
	Error interface{}
}

type InternalServerError struct {
	Code    string
	Message string
}
