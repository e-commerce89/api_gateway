package models

import "time"

type CategoryModel struct {
	Title       string     `json:"title"`
	Description string     `json:"description"`
	Picture     string     `json:"picture"`
	Parent_id   string     `json:"parent_id"`
	Status      bool       `json:"status"`
	Created_at  time.Time  `json:"created_at"`
	Updated_at  *time.Time `json:"updated_at"`
}

type CreateCategoryModel struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	Picture     string `json:"picture"`
	Parent_id   string `json:"parent_id"`
	Status      bool   `json:"status"`
}

type UpdateCategoryModel struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	Picture     string `json:"picture"`
	Parent_id   string `json:"parent_id"`
	Status      bool   `json:"status"`
}

type GetCategoryModel struct {
	ID string `json:"id"`
	CategoryModel
}

type GetAllCategoriesModel struct {
	Categories []GetCategoryModel `json:"categories"`
	Count      int32              `json:"count"`
}

type CreatedCategoryModel struct {
	CategoryID string `json:"category_id"`
}
