package v1

import (
	"database/sql"
	"encoding/json"
	"errors"
	"net/http"
	"regexp"
	"strconv"
	"time"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
	"github.com/xtgo/uuid"

	"github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/api/models"
	"github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/config"
	"github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/pkg/logger"
	"github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/service"
)

var (
	ErrAlreadyExists       = "ALREADY_EXISTS"
	ErrInvalidArgument     = "INVALID_ARGUMENT"
	ErrNotFound            = "NOT_FOUND"
	ErrInternalServerError = "INTERNAL_SERVER_ERROR"
	ErrServiceUnavailable  = "SERVICE_UNAVAILABLE"
	ErrUnknownError        = "UNKNOWN_ERROR"
	ErrUnauthorized        = "UNAUTHORIZED"
	ErrBadRequest          = "BAD_REQUEST"
	ErrCodeForbidden       = "FORBIDDEN"
)

type handlerV1 struct {
	log      logger.Logger
	cfg      *config.Config
	services service.ServiceManager
}

type HandlerV1Options struct {
	Log      logger.Logger
	Cfg      *config.Config
	Services service.ServiceManager
}

func New(options *HandlerV1Options) *handlerV1 {
	return &handlerV1{
		log:      options.Log,
		cfg:      options.Cfg,
		services: options.Services,
	}
}

func ParseOffsetQueryParam(c *gin.Context) (int32, error) {
	offset, err := strconv.ParseInt(c.DefaultQuery("offset", "0"), 10, 32)
	if err != nil {
		return 0, err
	}
	if offset <= 0 {
		return 0, nil
	}
	return int32(offset), nil
}

func ParseLimitQueryParam(c *gin.Context) (int32, error) {
	limit, err := strconv.ParseInt(c.DefaultQuery("limit", "10"), 10, 32)
	if err != nil {
		return 0, err
	}
	if limit <= 0 {
		return 10, nil
	}
	return int32(limit), nil
}

func (h *handlerV1) ReturnBadRequest(c *gin.Context, message string) {
	c.JSON(http.StatusBadRequest, gin.H{
		"error": message,
	})
}

func (h *handlerV1) IsUUID(c *gin.Context, id string) error {
	_, err := uuid.Parse(id)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return err
	}
	return nil
}

func GetClientIdFromHeader(c *gin.Context) string {
	return c.GetHeader("client-id")
}

func ParseQueryParam(c *gin.Context, key string, defaultValue string) (int, error) {
	valueStr := c.DefaultQuery(key, defaultValue)

	value, err := strconv.Atoi(valueStr)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		c.Abort()
		return 0, err
	}

	return value, nil
}
func handleDateValidation(date string) error {
	layout := "2006-01-02"

	_, err := time.Parse(layout, date)

	if err != nil {
		return err
	}
	return nil

}

func ValidatePhoneNumber(phoneNumber string) error {
	if phoneNumber == "" {
		return errors.New("phone_number is blank")
	}
	pattern := regexp.MustCompile(`^(\+[0-9]{12})$`)

	if !(pattern.MatchString(phoneNumber) && phoneNumber[0:4] == "+998") {
		return errors.New("phone_number must be +998XXXXXXXXX")
	}
	return nil
}

func ProtoToStruct(s interface{}, p proto.Message) error {
	var jm jsonpb.Marshaler

	jm.EmitDefaults = true
	jm.OrigName = true

	ms, err := jm.MarshalToString(p)

	if err != nil {
		return err
	}
	err = json.Unmarshal([]byte(ms), &s)

	return err
}

func handleError(log logger.Logger, c *gin.Context, err error, message string) (hasError bool) {
	st, ok := status.FromError(err)
	if st.Code() == codes.Unauthenticated {
		log.Error(message+", unauthorized", logger.Error(err))
		c.JSON(http.StatusUnauthorized, gin.H{
			"error":   ErrUnauthorized,
			"message": err.Error(),
		})
		return
	} else if st.Code() == codes.PermissionDenied {
		log.Error(message+", forbidden", logger.Error(err))
		c.JSON(http.StatusForbidden, gin.H{
			"error":   ErrCodeForbidden,
			"message": err.Error(),
		})
		return
	} else if st.Code() == codes.AlreadyExists {
		log.Error(message+", already exists", logger.Error(err))
		c.JSON(http.StatusBadRequest, gin.H{
			"error":   ErrAlreadyExists,
			"message": err.Error(),
		})
		return
	} else if st.Code() == codes.InvalidArgument {
		log.Error(message+", invalid argument", logger.Error(err))
		c.JSON(http.StatusBadRequest, gin.H{
			"error":   ErrInvalidArgument,
			"message": err.Error(),
		})
		return
	} else if st.Code() == codes.NotFound {
		log.Error(message+", not found", logger.Error(err))
		c.JSON(http.StatusNotFound, gin.H{
			"error":   ErrNotFound,
			"message": err.Error(),
		})
		return
	} else if st.Code() == codes.Unavailable {
		log.Error(message+", service unavailable", logger.Error(err))
		c.JSON(http.StatusServiceUnavailable, gin.H{
			"error":   ErrServiceUnavailable,
			"message": err.Error(),
		})
		return
	} else if !ok || st.Code() == codes.Internal || st.Code() == codes.Unknown {
		log.Error(message+", internal server error", logger.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{
			"error":   ErrInternalServerError,
			"message": err.Error(),
		})
		return
	}
	if err != nil || !ok || st.Code() == codes.Unknown {
		log.Error(message+", unknown error", logger.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{
			"error":   ErrUnknownError,
			"message": err.Error(),
		})
		return
	}
	return true
}

// handlings
func handleGrpcErrWithMessage(c *gin.Context, l logger.Logger, err error, message string) bool {
	st, ok := status.FromError(err)
	if !ok || st.Code() == codes.Internal {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrInternalServerError,
				Message: st.Message(),
			},
		})
		l.Error(message, logger.Error(err))
		return true
	}
	if st.Code() == codes.Unauthenticated {
		c.JSON(http.StatusForbidden, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrCodeForbidden,
				Message: st.Message(),
			},
		})
		l.Error(message, logger.Error(err))
		return true
	} else if st.Code() == codes.PermissionDenied {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrUnauthorized,
				Message: st.Message(),
			},
		})
		l.Error(message, logger.Error(err))
		return true
	} else if st.Code() == codes.NotFound {
		c.JSON(http.StatusNotFound, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrNotFound,
				Message: st.Message(),
			},
		})
		l.Error(message+", not found", logger.Error(err))
		return true
	} else if st.Code() == codes.Unavailable {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrInternalServerError,
				Message: "Internal Server Error",
			},
		})
		l.Error(message+", service unavailable", logger.Error(err))
		return true
	} else if st.Code() == codes.AlreadyExists {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrAlreadyExists,
				Message: st.Message(),
			},
		})
		l.Error(message+", already exists", logger.Error(err))
		return true
	} else if st.Code() == codes.InvalidArgument {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrBadRequest,
				Message: st.Message(),
			},
		})
		l.Error(message+", invalid field", logger.Error(err))
		return true
	} else if st.Code() == codes.Code(20) {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrBadRequest,
				Message: st.Message(),
			},
		})
		l.Error(message+", invalid field", logger.Error(err))
		return true
	} else if st.Err() != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrBadRequest,
				Message: st.Message(),
			},
		})
		l.Error(message+", invalid field", logger.Error(err))
		return true
	}
	return false
}
func handleInternalServerErrWithMessage(c *gin.Context, l logger.Logger, err error, message string) bool {
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrBadRequest,
				Message: message,
			},
		})
		l.Error(message, logger.Error(err))
		return true
	}
	return false
}

func handleBadRequestErrWithMessage(c *gin.Context, l logger.Logger, err error, message string) bool {
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrBadRequest,
				Message: message,
			},
		})
		l.Error(message, logger.Error(err))
		return true
	}
	return false
}

func handleStorageErrWithMessage(c *gin.Context, l logger.Logger, err error, message string) bool {
	if err == sql.ErrNoRows {
		c.JSON(http.StatusNotFound, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrNotFound,
				Message: "Not found",
			},
		})
		l.Error(message+", not found", logger.Error(err))
		return true
	} else if err == errors.New("Already exists") {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrAlreadyExists,
				Message: "Already Exists",
			},
		})
		l.Error(message+", already exists", logger.Error(err))
		return true
	} else if err != nil {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrInternalServerError,
				Message: "Internal Server Error",
			},
		})
		l.Error(message, logger.Error(err))
		return true
	}

	return false
}
