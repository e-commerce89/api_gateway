package v1

import (
	"context"
	"net/http"

	cart_service "github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/genproto/order_service"
	"github.com/gin-gonic/gin"
)

// @Router /v1/cart [POST]
// @Summary Create Cart
// @Description API for creating cart
// @Tags cart
// @Accept json
// @Produce json
// @Param cart body cart_service.CreateCartRequest true "cart"
// @Success 201 {object} cart_service.CreateCartResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) CreateCart(c *gin.Context) {
	var (
		createCart cart_service.CreateCartRequest
	)

	err := c.ShouldBindJSON(&createCart)
	if err != nil {
		h.ReturnBadRequest(c, err.Error())
		return
	}

	// _, err = h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	res, err := h.services.CartService().Create(context.Background(), &createCart)

	if !handleError(h.log, c, err, "error while creating cart") {
		return
	}

	c.JSON(http.StatusCreated, res)
}

// @Router /v1/cart/{cart_id} [GET]
// @Summary Get Cart
// @Description API for getting cart
// @Tags cart
// @Accept json
// @Produce json
// @Param product_id path string true "product_id"
// @Success 200 {object} cart_service.GetCartByIdResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) GetCart(c *gin.Context) {

	productId := c.Param("product_id")
	userIdRaw, ok := c.Get("user_id")
	if !ok {
		c.JSON(http.StatusUnauthorized, "Something is worng")
		return
	}
	userId, ok := userIdRaw.(string)
	if !ok {
		c.JSON(http.StatusUnauthorized, "Something is worng")
		return
	}
	if h.IsUUID(c, productId) != nil {
		return
	}
	res, err := h.services.CartService().GetById(
		context.Background(),
		&cart_service.GetCartByIdRequest{ProductId: productId, UserId: userId})

	if !handleError(h.log, c, err, "error while getting cart") {
		return
	}

	// err = ProtoToStruct(&cart, res)

	if err != nil {
		return
	}

	c.JSON(http.StatusOK, res)
}

// @Router /v1/cart/ [PUT]
// @Summary Update Cart
// @Description API for updating cart
// @Tags cart
// @Accept json
// @Produce json
// @Param cart body cart_service.UpdateCartRequest true "cart"
// @Success 204
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) UpdateCart(c *gin.Context) {
	var (
		cart cart_service.UpdateCartRequest
	)

	err := c.ShouldBindJSON(&cart)
	if err != nil {
		h.ReturnBadRequest(c, err.Error())
		return
	}

	// _, err = h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }
	_, err = h.services.CartService().Update(context.Background(), &cart)

	if !handleError(h.log, c, err, "error while updating cart") {
		return
	}

	c.Status(http.StatusNoContent)
}

// @Router /v1/cart [GET]
// @Summary Get All Carts
// @Description API for getting all carts
// @Tags cart
// @Accept json
// @Produce json
// @Param       offset        query    string        false "offset"
// @Param       limit         query    string        false "limit"
// @Param       userid        query    string        false "userid"
// @Success 200 {object} cart_service.GetCartListResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) GetAllCart(c *gin.Context) {

	offset, err := ParseQueryParam(c, "offset", h.cfg.DefaultOffset)
	if err != nil {
		return
	}

	limit, err := ParseQueryParam(c, "limit", h.cfg.DefaultLimit)
	if err != nil {
		return
	}
	res, err := h.services.CartService().GetList(context.Background(), &cart_service.GetCartListRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
		UserId: c.Query("userid"),
	})

	if err != nil {
		return
	}

	// err = ProtoToStruct(&categories, res)

	if !handleError(h.log, c, err, "error while getting all carts") {
		return
	}

	c.JSON(http.StatusOK, res)
}

// @Router /v1/cart/{product_id} [DELETE]
// @Summary Delete Cart
// @Description API for deleting cart
// @Tags cart
// @Accept json
// @Produce json
// @Param cart_id path string true "cart_id"
// @Success 204
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) DeleteCart(c *gin.Context) {

	// _, err := h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	cartID := c.Param("cart_id")

	if h.IsUUID(c, cartID) != nil {
		return
	}

	_, err := h.services.CartService().Delete(context.Background(), &cart_service.DeleteCartRequest{ProductId: cartID})

	if !handleError(h.log, c, err, "error while deleting cart") {
		return
	}

	c.Status(http.StatusNoContent)
}
