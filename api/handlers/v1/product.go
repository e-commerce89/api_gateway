package v1

import (
	"context"
	"net/http"
	catalog_service "github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/genproto/catalog_service"
	"github.com/gin-gonic/gin"
)

// @Router /v1/product [POST]
// @Summary Create Product
// @Description API for creating product
// @Tags product
// @Accept json
// @Produce json
// @Param product body catalog_service.CreateProductRequest true "product"
// @Success 201 {object} catalog_service.CreateProductResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) CreateProduct(c *gin.Context) {
	var (
		createProduct catalog_service.CreateProductRequest
	)
	err := c.ShouldBindJSON(&createProduct)
	if err != nil {
		h.ReturnBadRequest(c, err.Error())
		return
	}

	// _, err = h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	res, err := h.services.ProductService().Create(context.Background(), &createProduct)

	if !handleError(h.log, c, err, "error while creating product") {
		return
	}

	c.JSON(http.StatusCreated, res)
}

// @Router /v1/product/{product_id} [GET]
// @Summary Get Product
// @Description API for getting product
// @Tags product
// @Accept json
// @Produce json
// @Param product_id path string true "product_id"
// @Success 200 {object} catalog_service.ProductResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) GetProduct(c *gin.Context) {

	productID := c.Param("product_id")

	if h.IsUUID(c, productID) != nil {
		return
	}
	res, err := h.services.ProductService().GetByID(
		context.Background(),
		&catalog_service.GetByIDResquest{Id: productID})

	if !handleError(h.log, c, err, "error while getting product") {
		return
	}

	// err = ProtoToStruct(&product, res)

	if err != nil {
		return
	}

	c.JSON(http.StatusOK, res)
}

// @Router /v1/product/{product_id} [PUT]
// @Summary Update Product
// @Description API for updating product
// @Tags product
// @Accept json
// @Produce json
// @Param product_id path string true "product_id"
// @Param product body catalog_service.UpdateProductRequest true "product"
// @Success 204
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) UpdateProduct(c *gin.Context) {
	var (
		product catalog_service.UpdateProductRequest
	)

	err := c.ShouldBindJSON(&product)
	if err != nil {
		h.ReturnBadRequest(c, err.Error())
		return
	}

	// _, err = h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	productID := c.Param("product_id")

	if h.IsUUID(c, productID) != nil {
		return
	}

	product.Id = productID
	_, err = h.services.ProductService().Update(context.Background(), &product)

	if !handleError(h.log, c, err, "error while updating product") {
		return
	}

	c.Status(http.StatusNoContent)
}

// @Router /v1/product [GET]
// @Summary Get All Products
// @Description API for getting all products
// @Tags product
// @Accept json
// @Produce json
// @Param       offset        query    string        false "offset"
// @Param       limit         query    string        false "limit"
// @Param       search        query    string        false "search"
// @Param       category_id        query    string        false "category_id"
// @Success 200 {object} catalog_service.GetListProductResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) GetAllProducts(c *gin.Context) {
	
	offset, err := ParseQueryParam(c, "offset", h.cfg.DefaultOffset)
	if err != nil {
		return
	}

	limit, err := ParseQueryParam(c, "limit", h.cfg.DefaultLimit)
	if err != nil {
		return
	}
	res, err := h.services.ProductService().GetList(context.Background(), &catalog_service.GetListProductRequest{
		Offset:     int32(offset),
		Limit:      int32(limit),
		Search:     c.Query("search"),
		CategoryId: c.Query("category_id"),
	})

	if err != nil {
		return
	}

	// err = ProtoToStruct(&categories, res)

	if !handleError(h.log, c, err, "error while getting all products") {
		return
	}

	c.JSON(http.StatusOK, res)
}

// @Router /v1/product/{product_id} [DELETE]
// @Summary Delete Product
// @Description API for deleting product
// @Tags product
// @Accept json
// @Produce json
// @Param product_id path string true "product_id"
// @Success 204
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) DeleteProduct(c *gin.Context) {

	// _, err := h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	productID := c.Param("product_id")

	if h.IsUUID(c, productID) != nil {
		return
	}

	_, err := h.services.ProductService().Delete(context.Background(), &catalog_service.DeleteRequest{Id: productID})

	if !handleError(h.log, c, err, "error while deleting product") {
		return
	}

	c.Status(http.StatusNoContent)
}
