package v1

import (
	"context"
	"net/http"

	"github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/api/models"
	order_service "github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/genproto/order_service"
	"github.com/gin-gonic/gin"
)

// @Router /v1/order [POST]
// @Summary Create Order
// @Description API for creating order
// @Tags order
// @Accept json
// @Produce json
// @Param order body order_service.CreateOrderRequest true "order"
// @Success 201 {object} order_service.CreateOrderResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) CreateOrder(c *gin.Context) {
	var (
		createOrder models.CreateOrderModel
	)

	err := c.ShouldBindJSON(&createOrder)
	if err != nil {
		h.ReturnBadRequest(c, err.Error())
		return
	}
	userIdRaw, ok := c.Get("auth_user_id")
	if !ok {
		c.JSON(http.StatusUnauthorized, "Something is worng")
		return
	}
	userId, ok := userIdRaw.(string)
	if !ok {
		c.JSON(http.StatusUnauthorized, "Something is worng")
		return
	}
	ordI := make([]*order_service.OrderItem, 0)
	for _, v := range createOrder.OrderItems {
		ordI = append(ordI, &order_service.OrderItem{
			ProductId: v.ProductId,
			Quantity:  int32(v.Quantity),
		})
	}
	// _, err = h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	res, err := h.services.OrderService().Create(context.Background(), &order_service.CreateOrderRequest{
		UserId:          userId,
		CustomerName:    createOrder.CustomerName,
		CustomerAddress: createOrder.CustomerAddress,
		CustomerPhone:   createOrder.CustomerPhone,
		Orderitems:      ordI,
		PaymentMethod: createOrder.PaymentMethod,
		PaymentStatus: createOrder.PaymentStatus,
	})

	if !handleError(h.log, c, err, "error while creating order") {
		return
	}

	c.JSON(http.StatusCreated, res)
}

// @Router /v1/order/{order_id} [GET]
// @Summary Get Order
// @Description API for getting order
// @Tags order
// @Accept json
// @Produce json
// @Param order_id path string true "order_id"
// @Success 200 {object} order_service.GetOrderByIdResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) GetOrder(c *gin.Context) {

	orderID := c.Param("order_id")

	if h.IsUUID(c, orderID) != nil {
		return
	}
	userIdRaw, ok := c.Get("auth_user_id")
	if !ok {
		c.JSON(http.StatusUnauthorized, "Something is worng")
		return
	}
	userId, ok := userIdRaw.(string)
	if !ok {
		c.JSON(http.StatusUnauthorized, "Something is worng")
		return
	}
	res, err := h.services.OrderService().GetById(
		context.Background(),
		&order_service.GetOrderByIdRequest{Id: orderID,UserId: userId})

	if !handleError(h.log, c, err, "error while getting order") {
		return
	}

	// err = ProtoToStruct(&order, res)

	if err != nil {
		return
	}

	c.JSON(http.StatusOK, res)
}

// @Router /v1/order/{order_id} [PUT]
// @Summary Update Order
// @Description API for updating order
// @Tags order
// @Accept json
// @Produce json
// @Param order_id path string true "order_id"
// @Param order body order_service.UpdateOrderRequest true "order"
// @Success 204
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) UpdateOrder(c *gin.Context) {
	var (
		order order_service.UpdateOrderRequest
	)

	err := c.ShouldBindJSON(&order)
	if err != nil {
		h.ReturnBadRequest(c, err.Error())
		return
	}

	// _, err = h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	orderID := c.Param("order_id")

	if h.IsUUID(c, orderID) != nil {
		return
	}

	order.Id = orderID
	_, err = h.services.OrderService().Update(context.Background(), &order)

	if !handleError(h.log, c, err, "error while updating order") {
		return
	}

	c.Status(http.StatusNoContent)
}

// @Router /v1/order [GET]
// @Summary Get All Orders
// @Description API for getting all orders
// @Tags order
// @Accept json
// @Produce json
// @Param       offset        query    string        false "offset"
// @Param       limit         query    string        false "limit"
// @Param       search        query    string        false "search"
// @Param       userid        query    string        false "userid"
// @Success 200 {object} order_service.GetOrderListResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) GetAllOrder(c *gin.Context) {

	offset, err := ParseQueryParam(c, "offset", h.cfg.DefaultOffset)
	if err != nil {
		return
	}

	limit, err := ParseQueryParam(c, "limit", h.cfg.DefaultLimit)
	if err != nil {
		return
	}
	res, err := h.services.OrderService().GetList(context.Background(), &order_service.GetOrderListRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
		Search: c.Query("search"),
		UserId: c.Query("userid"),
	})

	if err != nil {
		return
	}

	// err = ProtoToStruct(&categories, res)

	if !handleError(h.log, c, err, "error while getting all orders") {
		return
	}

	c.JSON(http.StatusOK, res)
}

// @Router /v1/order/{order_id} [DELETE]
// @Summary Delete Order
// @Description API for deleting order
// @Tags order
// @Accept json
// @Produce json
// @Param order_id path string true "order_id"
// @Success 204
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) DeleteOrder(c *gin.Context) {

	// _, err := h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	orderID := c.Param("order_id")

	if h.IsUUID(c, orderID) != nil {
		return
	}

	_, err := h.services.OrderService().Delete(context.Background(), &order_service.DeleteOrderRequest{Id: orderID})

	if !handleError(h.log, c, err, "error while deleting order") {
		return
	}

	c.Status(http.StatusNoContent)
}
