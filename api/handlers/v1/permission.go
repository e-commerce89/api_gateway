package v1

import (
	"context"
	"net/http"
	auth_service "github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/genproto/auth_service"
	"github.com/gin-gonic/gin"
)

// @Router /v1/permission [POST]
// @Summary Create Permission
// @Description API for creating permission
// @Tags permission
// @Accept json
// @Produce json
// @Param permission body auth_service.CreatePermissionRequest true "permission"
// @Success 201 {object} auth_service.CreatePermissionResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) CreatePermission(c *gin.Context) {
	var (
		createPermission auth_service.CreatePermissionRequest
	)
	err := c.ShouldBindJSON(&createPermission)
	if err != nil {
		h.ReturnBadRequest(c, err.Error())
		return
	}

	// _, err = h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	res, err := h.services.PermissionService().Create(context.Background(), &createPermission)

	if !handleError(h.log, c, err, "error while creating permission") {
		return
	}

	c.JSON(http.StatusCreated, res)
}


// @Router /v1/permission/{permission_id} [PUT]
// @Summary Update Permission
// @Description API for updating permission
// @Tags permission
// @Accept json
// @Produce json
// @Param permission body auth_service.Permission true "permission"
// @Success 204
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) UpdatePermission(c *gin.Context) {
	var (
		permission auth_service.Permission
	)

	err := c.ShouldBindJSON(&permission)
	if err != nil {
		h.ReturnBadRequest(c, err.Error())
		return
	}

	// _, err = h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }


	if h.IsUUID(c, permission.Id) != nil {
		return
	}

	_, err = h.services.PermissionService().Update(context.Background(), &permission)

	if !handleError(h.log, c, err, "error while updating permission") {
		return
	}

	c.Status(http.StatusNoContent)
}

// @Router /v1/permission [GET]
// @Summary Get All Permissions
// @Description API for getting all permissions
// @Tags permission
// @Accept json
// @Produce json
// @Param       offset        query    string        false "offset"
// @Param       limit         query    string        false "limit"
// @Success 200 {object} auth_service.PermissionsResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) GetAllPermissions(c *gin.Context) {
	
	offset, err := ParseQueryParam(c, "offset", h.cfg.DefaultOffset)
	if err != nil {
		return
	}

	limit, err := ParseQueryParam(c, "limit", h.cfg.DefaultLimit)
	if err != nil {
		return
	}
	res, err := h.services.PermissionService().GetAll(context.Background(), &auth_service.GetPermissionRequest{
		Offset:     int32(offset),
		Limit:      int32(limit),
	})

	if err != nil {
		return
	}

	// err = ProtoToStruct(&categories, res)

	if !handleError(h.log, c, err, "error while getting all permissions") {
		return
	}

	c.JSON(http.StatusOK, res)
}

// @Router /v1/permission/{permission_id} [DELETE]
// @Summary Delete Permission
// @Description API for deleting permission
// @Tags permission
// @Accept json
// @Produce json
// @Param permission_id path string true "permission_id"
// @Success 204
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) DeletePermission(c *gin.Context) {

	// _, err := h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	permissionID := c.Param("permission_id")

	if h.IsUUID(c, permissionID) != nil {
		return
	}

	_, err := h.services.PermissionService().Delete(context.Background(), &auth_service.DeletePermissionRequest{Id: permissionID})

	if !handleError(h.log, c, err, "error while deleting permission") {
		return
	}

	c.Status(http.StatusNoContent)
}
