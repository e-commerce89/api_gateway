package v1

import (
	"context"
	"net/http"
	auth_service "github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/genproto/auth_service"
	"github.com/gin-gonic/gin"
)

// @Router /v1/user_role [POST]
// @Summary Create UserRole
// @Description API for creating user_role
// @Tags user_role
// @Accept json
// @Produce json
// @Param user_role body auth_service.CreateUserRole true "user_role"
// @Success 201 {object} auth_service.CreateUserRoleResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) CreateUserRole(c *gin.Context) {
	var (
		createUserRole auth_service.CreateUserRole
	)
	err := c.ShouldBindJSON(&createUserRole)
	if err != nil {
		h.ReturnBadRequest(c, err.Error())
		return
	}

	// _, err = h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	res, err := h.services.UserRoleService().Create(context.Background(), &createUserRole)

	if !handleError(h.log, c, err, "error while creating user_role") {
		return
	}

	c.JSON(http.StatusCreated, res)
}


// @Router /v1/user_role/{user_role_id} [PUT]
// @Summary Update UserRole
// @Description API for updating user_role
// @Tags user_role
// @Accept json
// @Produce json
// @Param user_role body auth_service.UserRole true "user_role"
// @Success 204
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) UpdateUserRole(c *gin.Context) {
	var (
		user_role auth_service.UserRole
	)

	err := c.ShouldBindJSON(&user_role)
	if err != nil {
		h.ReturnBadRequest(c, err.Error())
		return
	}

	// _, err = h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }


	if h.IsUUID(c, user_role.Id) != nil {
		return
	}

	_, err = h.services.UserRoleService().Update(context.Background(), &user_role)

	if !handleError(h.log, c, err, "error while updating user_role") {
		return
	}

	c.Status(http.StatusNoContent)
}

// @Router /v1/user_role [GET]
// @Summary Get All UserRoles
// @Description API for getting all user_roles
// @Tags user_role
// @Accept json
// @Produce json
// @Param       offset        query    string        false "offset"
// @Param       limit         query    string        false "limit"
// @Success 200 {object} auth_service.UserRolesResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) GetAllUserRoles(c *gin.Context) {
	
	offset, err := ParseQueryParam(c, "offset", h.cfg.DefaultOffset)
	if err != nil {
		return
	}

	limit, err := ParseQueryParam(c, "limit", h.cfg.DefaultLimit)
	if err != nil {
		return
	}
	res, err := h.services.UserRoleService().GetAll(context.Background(), &auth_service.GetAllRequest{
		Offset:     int32(offset),
		Limit:      int32(limit),
	})

	if err != nil {
		return
	}

	// err = ProtoToStruct(&categories, res)

	if !handleError(h.log, c, err, "error while getting all user_roles") {
		return
	}

	c.JSON(http.StatusOK, res)
}

// @Router /v1/user_role/{user_role_id} [DELETE]
// @Summary Delete UserRole
// @Description API for deleting user_role
// @Tags user_role
// @Accept json
// @Produce json
// @Param user_role_id path string true "user_role_id"
// @Success 204
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) DeleteUserRole(c *gin.Context) {

	// _, err := h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	user_roleID := c.Param("user_role_id")

	if h.IsUUID(c, user_roleID) != nil {
		return
	}

	_, err := h.services.UserRoleService().Delete(context.Background(), &auth_service.DeleteRequest{Id: user_roleID})

	if !handleError(h.log, c, err, "error while deleting user_role") {
		return
	}

	c.Status(http.StatusNoContent)
}
