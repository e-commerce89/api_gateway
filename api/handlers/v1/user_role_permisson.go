package v1

import (
	"context"
	"net/http"
	auth_service "github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/genproto/auth_service"
	"github.com/gin-gonic/gin"
)

// @Router /v1/user_role_permission [POST]
// @Summary Create UserRolePermission
// @Description API for creating user_role_permission
// @Tags user_role_permission
// @Accept json
// @Produce json
// @Param user_role_permission body auth_service.CreateUserRolePermission true "user_role_permission"
// @Success 201 {object} auth_service.UserRolePermissionResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) CreateUserRolePermission(c *gin.Context) {
	var (
		createUserRolePermission auth_service.CreateUserRolePermission
	)
	err := c.ShouldBindJSON(&createUserRolePermission)
	if err != nil {
		h.ReturnBadRequest(c, err.Error())
		return
	}

	// _, err = h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	res, err := h.services.UserRolePermissionService().Create(context.Background(), &createUserRolePermission)

	if !handleError(h.log, c, err, "error while creating user_role_permission") {
		return
	}

	c.JSON(http.StatusCreated, res)
}

// @Router /v1/user_role_permission [GET]
// @Summary Get All UserRolePermissions
// @Description API for getting all user_role_permissions
// @Tags user_role_permission
// @Accept json
// @Produce json
// @Param       user_role_id         query    string        false "user_role_id"
// @Success 200 {object} auth_service.UserRolePermissionsResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) GetAllUserRolePermissions(c *gin.Context) {
	user_roleID := c.Param("user_role_id")

	if h.IsUUID(c, user_roleID) != nil {
		return
	}
	res, err := h.services.UserRolePermissionService().GetList(context.Background(), &auth_service.GetUserRolePermissionRequest{
		UserRoleId: user_roleID,
	})

	if err != nil {
		return
	}

	// err = ProtoToStruct(&categories, res)

	if !handleError(h.log, c, err, "error while getting all user_role_permissions") {
		return
	}

	c.JSON(http.StatusOK, res)
}

// @Router /v1/user_role_permission/{user_role_permission_id} [DELETE]
// @Summary Delete UserRolePermission
// @Description API for deleting user_role_permission
// @Tags user_role_permission
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param user_role_id path string true "user_role_id"
// @Success 204
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) DeleteUserRolePermission(c *gin.Context) {

	// _, err := h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	id := c.Param("id")

	if h.IsUUID(c, id) != nil {
		return
	}


	user_role_id := c.Param("user_role_id")

	if h.IsUUID(c, id) != nil {
		return
	}
	_, err := h.services.UserRolePermissionService().Delete(context.Background(), &auth_service.DeleteUserRolePermissionRequest{Id: id,UserRoleId: user_role_id})

	if !handleError(h.log, c, err, "error while deleting user_role_permission") {
		return
	}

	c.Status(http.StatusNoContent)
}
