package v1

import (
	"net/http"

	"github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/config"
	auth_service "github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/genproto/auth_service"
	"github.com/gin-gonic/gin"
)

func (h handlerV1) AuthMiddleware(userType string) gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.GetHeader("Authorization")
		hasAccessResponse, err := h.services.AuthService().HasAccess(c.Request.Context(), &auth_service.TokenRequest{
			Token: token,
		})
		if err != nil {
			c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"Error": err.Error(),
			})
			c.Abort()
			return
		}

		if !hasAccessResponse.HasAccess {
			c.JSON(http.StatusUnauthorized, "Unauthorized")
			c.Abort()
			return
		}

		if userType != "*" {
			if userType=="SUPERUSER" && hasAccessResponse.UserRoleId != config.SuperUserId {
				c.JSON(http.StatusUnauthorized, "Permission Denied")
				c.Abort()
			}
		}
		c.Set("auth_user_type", hasAccessResponse.UserRoleId)
		c.Set("auth_user_id", hasAccessResponse.UserId)

		c.Next()
	}
}


