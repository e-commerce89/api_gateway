package v1

import (
	"context"
	"net/http"
	auth_service "github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/genproto/auth_service"
	"github.com/gin-gonic/gin"
)

// @Router /v1/user [POST]
// @Summary Create User
// @Description API for creating user
// @Tags user
// @Accept json
// @Produce json
// @Param user body auth_service.CreateUserRequest true "user"
// @Success 201 {object} auth_service.User
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) CreateUser(c *gin.Context) {
	var (
		createUser auth_service.CreateUserRequest
	)
	err := c.ShouldBindJSON(&createUser)
	if err != nil {
		h.ReturnBadRequest(c, err.Error())
		return
	}

	// _, err = h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	res, err := h.services.AuthService().CreateUser(context.Background(), &createUser)

	if !handleError(h.log, c, err, "error while creating user") {
		return
	}

	c.JSON(http.StatusCreated, res)
}

// @Router /v1/user/{user_id} [GET]
// @Summary Get User
// @Description API for getting user
// @Tags user
// @Accept json
// @Produce json
// @Param user_id path string true "user_id"
// @Success 200 {object} auth_service.User
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) GetUser(c *gin.Context) {

	userID := c.Param("user_id")

	if h.IsUUID(c, userID) != nil {
		return
	}
	res, err := h.services.AuthService().GetUserById(
		context.Background(),
		&auth_service.GetUserByIdRequest{Id: userID})

	if !handleError(h.log, c, err, "error while getting user") {
		return
	}

	// err = ProtoToStruct(&user, res)

	if err != nil {
		return
	}

	c.JSON(http.StatusOK, res)
}

// @Router /v1/user/{user_id} [PUT]
// @Summary Update User
// @Description API for updating user
// @Tags user
// @Accept json
// @Produce json
// @Param user_id path string true "user_id"
// @Param user body auth_service.UpdateUserRequest true "user"
// @Success 204
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) UpdateUser(c *gin.Context) {
	var (
		user auth_service.UpdateUserRequest
	)

	err := c.ShouldBindJSON(&user)
	if err != nil {
		h.ReturnBadRequest(c, err.Error())
		return
	}

	// _, err = h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	userID := c.Param("user_id")

	if h.IsUUID(c, userID) != nil {
		return
	}

	user.Id = userID
	_, err = h.services.AuthService().UpdateUser(context.Background(), &user)

	if !handleError(h.log, c, err, "error while updating user") {
		return
	}

	c.Status(http.StatusNoContent)
}

// @Router /v1/user [GET]
// @Summary Get All Users
// @Description API for getting all users
// @Tags user
// @Accept json
// @Produce json
// @Param       offset        query    string        false "offset"
// @Param       limit         query    string        false "limit"
// @Param       search        query    string        false "search"
// @Success 200 {object} auth_service.GetUserListResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) GetAllUsers(c *gin.Context) {
	
	offset, err := ParseQueryParam(c, "offset", h.cfg.DefaultOffset)
	if err != nil {
		return
	}

	limit, err := ParseQueryParam(c, "limit", h.cfg.DefaultLimit)
	if err != nil {
		return
	}
	res, err := h.services.AuthService().GetUserList(context.Background(), &auth_service.GetUserListRequest{
		Offset:     int32(offset),
		Limit:      int32(limit),
		Search:     c.Query("search"),
	})

	if err != nil {
		return
	}

	// err = ProtoToStruct(&categories, res)

	if !handleError(h.log, c, err, "error while getting all users") {
		return
	}

	c.JSON(http.StatusOK, res)
}

// @Router /v1/user/{user_id} [DELETE]
// @Summary Delete User
// @Description API for deleting user
// @Tags user
// @Accept json
// @Produce json
// @Param user_id path string true "user_id"
// @Success 204
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) DeleteUser(c *gin.Context) {

	// _, err := h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	userID := c.Param("user_id")

	if h.IsUUID(c, userID) != nil {
		return
	}

	_, err := h.services.AuthService().DeleteUser(context.Background(), &auth_service.DeleteUserRequest{Id: userID})

	if !handleError(h.log, c, err, "error while deleting user") {
		return
	}

	c.Status(http.StatusNoContent)
}
// @Summary     Register User
// @Description Register User
// @Tags        user
// @Accept      json
// @Produce     json
// @Success 200 {object} auth_service.User
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
// @Router      /register [POST]
func (h *handlerV1) Register(c *gin.Context) {
	var user auth_service.RegisterUserRequest

	err := c.BindJSON(&user)
	if err != nil {
		h.ReturnBadRequest(c, err.Error())
		return
	}

	res, err := h.services.AuthService().Register(
		context.Background(),
		&user,
	)

	if !handleError(h.log, c, err, "error while register user") {
		return
	}

	c.JSON(http.StatusOK, res)
}

// Login User godoc
// @ID          login_user
// @Summary     Login User
// @Description Login User
// @Tags        user
// @Accept      json
// @Produce     json
// @Success 200 {object} auth_service.TokenResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
// @Router      /login [POST]
func (h *handlerV1) Login(c *gin.Context) {
	var login auth_service.LoginRequest

	err := c.BindJSON(&login)
	if err != nil {
		h.ReturnBadRequest(c, err.Error())
		return
	}

	res, err := h.services.AuthService().Login(
		context.Background(),
		&login,
	)

	if !handleError(h.log, c, err, "error while login user") {
		return
	}

	c.JSON(http.StatusOK, res)
}
