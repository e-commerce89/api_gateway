package v1

import (
	"context"
	"net/http"

	"github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/api/models"
	catalog_service "github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/genproto/catalog_service"
	"github.com/gin-gonic/gin"
)

// @Router /v1/category [POST]
// @Summary Create Category
// @Description API for creating category
// @Tags category
// @Accept json
// @Produce json
// @Param category body models.CreateCategoryModel true "category"
// @Success 201 {object} models.CreatedCategoryModel
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) CreateCategory(c *gin.Context) {
	var (
		createCategory catalog_service.CreateCategoryRequest
	)
	err := c.ShouldBindJSON(&createCategory)
	if err != nil {
		h.ReturnBadRequest(c, err.Error())
		return
	}

	// _, err = h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	res, err := h.services.CategoryService().Create(context.Background(), &createCategory)

	if !handleError(h.log, c, err, "error while creating category") {
		return
	}

	c.JSON(http.StatusCreated, res)
}

// @Router /v1/category/{category_id} [GET]
// @Summary Get Category
// @Description API for getting category
// @Tags category
// @Accept json
// @Produce json
// @Param category_id path string true "category_id"
// @Success 200 {object} models.GetCategoryModel
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) GetCategory(c *gin.Context) {
	var (
		category models.GetCategoryModel
	)
	categoryID := c.Param("category_id")

	if h.IsUUID(c, categoryID) != nil {
		return
	}
	res, err := h.services.CategoryService().GetByID(
		context.Background(),
		&catalog_service.GetByIDResquest{Id: categoryID})

	if !handleError(h.log, c, err, "error while getting category") {
		return
	}

	err = ProtoToStruct(&category, res)

	if err != nil {
		return
	}

	c.JSON(http.StatusOK, category)
}

// @Router /v1/category/{category_id} [PUT]
// @Summary Update Category
// @Description API for updating category
// @Tags category
// @Accept json
// @Produce json
// @Param category_id path string true "category_id"
// @Param category body models.UpdateCategoryModel true "category"
// @Success 204
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) UpdateCategory(c *gin.Context) {
	var (
		category catalog_service.UpdateCategoryRequest
	)

	err := c.ShouldBindJSON(&category)
	if err != nil {
		h.ReturnBadRequest(c, err.Error())
		return
	}

	// _, err = h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	categoryID := c.Param("category_id")

	if h.IsUUID(c, categoryID) != nil {
		return
	}

	category.Id = categoryID
	_, err = h.services.CategoryService().Update(context.Background(), &category)

	if !handleError(h.log, c, err, "error while updating category") {
		return
	}

	c.Status(http.StatusNoContent)
}

// @Router /v1/category [GET]
// @Summary Get All Categories
// @Description API for getting all categories
// @Tags category
// @Accept json
// @Produce json
// @Param       offset        query    string        false "offset"
// @Param       limit         query    string        false "limit"
// @Param       search        query    string        false "search"
// @Success 200 {object} models.GetAllCategoriesModel
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) GetAllCategories(c *gin.Context) {
	var (
		categories models.GetAllCategoriesModel
	)
	offset, err := ParseQueryParam(c, "offset", h.cfg.DefaultOffset)
	if err != nil {
		return
	}

	limit, err := ParseQueryParam(c, "limit", h.cfg.DefaultLimit)
	if err != nil {
		return
	}
	res, err := h.services.CategoryService().GetList(context.Background(), &catalog_service.GetListRequest{
		Offset: int32(offset),
		Limit: int32(limit),
		Search:   c.Query("search"),
	})

	if err != nil {
		return
	}

	err = ProtoToStruct(&categories, res)

	if !handleError(h.log, c, err, "error while getting all categories") {
		return
	}

	c.JSON(http.StatusOK, categories)
}

// @Router /v1/category/{category_id} [DELETE]
// @Summary Delete Category
// @Description API for deleting category
// @Tags category
// @Accept json
// @Produce json
// @Param category_id path string true "category_id"
// @Success 204
// @Failure 400 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Failure 503 {object} models.FailureResponse
func (h *handlerV1) DeleteCategory(c *gin.Context) {

	// _, err := h.GetAuthInfoFromToken(c)
	// if err != nil {
	// 	return
	// }

	categoryID := c.Param("category_id")

	if h.IsUUID(c, categoryID) != nil {
		return
	}

	_, err := h.services.CategoryService().Delete(context.Background(), &catalog_service.DeleteRequest{Id: categoryID})

	if !handleError(h.log, c, err, "error while deleting category") {
		return
	}

	c.Status(http.StatusNoContent)
}
