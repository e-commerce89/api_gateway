package api

import (
	_ "github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/api/docs"
	v1 "github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/api/handlers/v1"
	"github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/config"
	"github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/pkg/logger"
	"github.com/AbdulahadAbduqahhorov/E-commerce/api_gateway/service"
	"github.com/gin-gonic/gin"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

type RouterOptions struct {
	Log      logger.Logger
	Cfg      *config.Config
	Services service.ServiceManager
}

func New(opt *RouterOptions) *gin.Engine {

	router := gin.New()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	router.Use(MyCORSMiddleware())

	handlerV1 := v1.New(&v1.HandlerV1Options{
		Log:      opt.Log,
		Cfg:      opt.Cfg,
		Services: opt.Services,
	})

	apiV1 := router.Group("/v1")
	apiV1.Use()
	{
		apiV1.POST("/category", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.CreateCategory)
		apiV1.GET("/category", handlerV1.GetAllCategories)
		apiV1.GET("/category/:category_id", handlerV1.GetCategory)
		apiV1.PUT("/category/:category_id", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.UpdateCategory)
		apiV1.DELETE("/category/:category_id", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.DeleteCategory)

		apiV1.POST("/order", handlerV1.AuthMiddleware("*"), handlerV1.CreateOrder)
		apiV1.GET("/order/:order_id", handlerV1.AuthMiddleware("*"), handlerV1.GetOrder)
		apiV1.GET("/order", handlerV1.AuthMiddleware("*"), handlerV1.GetAllOrder)
		apiV1.PUT("/order/:order_id", handlerV1.AuthMiddleware("*"), handlerV1.UpdateOrder)
		apiV1.DELETE("/order/:order_id", handlerV1.AuthMiddleware("*"), handlerV1.DeleteOrder)

		apiV1.POST("/product", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.CreateProduct)
		apiV1.GET("/product", handlerV1.GetAllProducts)
		apiV1.GET("/product/:product_id", handlerV1.GetProduct)
		apiV1.PUT("/product/:product_id", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.UpdateProduct)
		apiV1.DELETE("/product/:product_id", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.DeleteProduct)

		apiV1.POST("/user", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.CreateUser)
		apiV1.GET("/user", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.GetAllUsers)
		apiV1.GET("/user/:user_id", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.GetUser)
		apiV1.PUT("/user/:user_id", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.UpdateUser)
		apiV1.DELETE("/user/:user_id", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.DeleteUser)
		apiV1.POST("/register", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.Register)
		apiV1.POST("/login", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.Login)

		apiV1.POST("/permission", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.CreatePermission)
		apiV1.GET("/permission", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.GetAllPermissions)
		apiV1.PUT("/permission/:permission_id", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.UpdatePermission)
		apiV1.DELETE("/permission/:permission_id", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.DeletePermission)

		apiV1.POST("/user_role", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.CreateUserRole)
		apiV1.GET("/user_role", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.GetAllUserRoles)
		apiV1.PUT("/user_role/:user_role_id", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.UpdateUserRole)
		apiV1.DELETE("/user_role/:user_role_id", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.DeleteUserRole)

		apiV1.POST("/user_role_permission", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.CreateUserRolePermission)
		apiV1.GET("/user_role_permission", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.GetAllUserRolePermissions)
		apiV1.DELETE("/user_role_permission/:user_role_permission_id", handlerV1.AuthMiddleware("SUPER_USER"), handlerV1.DeleteUserRolePermission)

	}
	url := ginSwagger.URL("/swagger/doc.json")
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return router
}

func MyCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE")
		c.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
